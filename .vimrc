" Pathogen 
call pathogen#infect()
call pathogen#helptags()

filetype plugin indent on
syntax on

set nu

colorscheme denkai

autocmd VimEnter * NERDTree
autocmd VimEnter * wincmd p

let g:NERDTreeWinPos = "right"
let NERDTreeShowHidden=1

nmap <silent> <F3> : wincmd p<CR>
nmap <silent> <F2> : NERDTreeToggle<CR>
nmap <silent> <TAB> : bNext<CR>

set noswapfile
set nobackup
set backspace=indent,eol,start

